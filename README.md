<a href="https://unige.it/en/">
<img src="images/genoa_logo.png" width="20%" height="20%" title="University of Genoa" alt="University of Genoa" >
</a> <img src="images/the_engine_room_logo.png" width="10%" height="10%" title="TheEngineRoom Lab" alt="TheEngineRoom Lab" >

> **Author: Omotoye Shamsudeen Adekoya**  
>  **Email: adekoyaomotoye@gmail.com**  
>  **Student ID: 5066348**

# Final Thesis Project: A teleoperation system for mobile manipulators

<div align="center"> 
<img src="images/tiago++.png" alt="Tiago++ Robot" height="30%", width="30%"/>
</div>

## Proposed Architecture:

The architecture would be frequently updated as the project progresses

![Tiago Teleoperation Architecture](images/UML%20Diagrams/Tiago%20Teleoperation%20Architecture-v2.png)

## Literature Review (Readings)

**Status**

- New Addition -> :question:
- Reading -> :construction:
- Read and Rejected -> :x:
- Read and Accepted -> :white_check_mark:

| Title                                                                                                                                          |     Status     |
| :--------------------------------------------------------------------------------------------------------------------------------------------- | :------------: |
| [[1]](#1) Mixed reality-integrated 3D/2D vision mapping for intuitive teleoperation of mobile manipulator                                      | :construction: |
| [[2]](#2) Whole-body multi-modal semi-autonomous teleoperation of mobile manipulator systems                                                   | :construction: |
| [[3]](#3) Switching control signal for bilateral tele-operation of a mobile manipulator                                                        | :construction: |
| [[4]](#4) Towards the development of an intuitive teleoperation system for human support robot using a VR device                               | :construction: |
| [[5]](#5) Real-time gesture recognition for the high-level teleoperation interface of a mobile manipulator                                     | :construction: |
| [[6]](#6) A state-of-the-art survey of Digital Twin: techniques, engineering product lifecycle management and business innovation perspectives | :construction: |
| [[7]](#7) Remote Robot Control with Human-in-the-Loop over Long Distances Using Digital Twins                                                  | :construction: |
| [[8]](#8) DIGITAL TWIN BASED SYNCHRONISED CONTROL AND SIMULATION OF THE INDUSTRIAL ROBOTIC CELL USING VIRTUAL REALITY                          | :construction: |
| [[9]](#9) GraspLook: a VR-based Telemanipulation System with R-CNN-driven Augmentation of Virtual Environment                                  | :construction: |
| [[10]](#10) An Auto-Correction Teleoperation Method for a Mobile Manipulator Using Gaze Tracking and Hand Motion Detection                     | :construction: |
| [[11]](#11) Mixed Reality Interface for Improving Mobile Manipulator Teleoperation in Contamination Critical Applications                      | :construction: |
| [[12]](#12) Inertial motion capture based reference trajectory generation for a mobile manipulator                                             | :construction: |
| [[13]](#13) Online Human Gesture Recognition using Recurrent Neural Networks and Wearable Sensors                                              | :construction: |
| [[14]](#14) Gesture recognition system for real-time mobile robot control based on inertial sensors and motion strings                         | :construction: |
| [[15]](#15) A Study on Mobile Robot Control by Hand gesture Detection                                                                          | :construction: |
| [[16]](#16) Mobile manipulator control through gesture recognition using IMUs and Online Lazy Neighborhood Graph search                        | :construction: |
| [[17]](#17) Object Detection and Gesture Control of Four-Wheel Mobile Robot                                                                    | :construction: |
| [[18]](#18) Adaptive Gesture Recognition System for Robotic Control using Surface EMG Sensors                                                  | :construction: |
| [[19]](#19) A Gesture Based Interface for Human-Robot Interaction                                                                              | :construction: |
| [[20]](#20) Intelligent Multi-fingered Dexterous Hand Using Virtual Reality (VR) and Robot Operating System (ROS)                              | :construction: |
| [[21]](#21) Mobile manipulator control based on voice and visual signal                                                                        |      :x:       |
| [[22]](#22) Gesture Controlled Bomb Diffusing Mobile Robot                                                                                     | :construction: |
| [[23]](#23) Gesture-based robot control: Design challenges and evaluation with humans                                                          | :construction: |
| [[24]](#24) Gesture-Based Human-Machine Interaction: Taxonomy, Problem Definition, and Analysis                                                | :construction: |

## References

<a id="1">[1]</a>Y. Su, X. Chen, T. Zhou, C. Pretty, and G. Chase, “Mixed reality-integrated 3D/2D vision mapping for intuitive teleoperation of mobile manipulator,” Robotics and Computer-Integrated Manufacturing, vol. 77, p. 102332, Oct. 2022, doi: 10.1016/j.rcim.2022.102332.

<a id="2">[2]</a>ChangSu Ha et al., “Whole-body multi-modal semi-autonomous teleoperation of mobile manipulator systems,” 2015 IEEE International Conference on Robotics and Automation (ICRA), May 2015, doi: 10.1109/icra.2015.7138995.

<a id="3">[3]</a>V. H. Andaluz, L. Salinas, F. Roberti, J. M. Toibero, and R. Carelli, “Switching control signal for bilateral tele-operation of a mobile manipulator,” 2011 9th IEEE International Conference on Control and Automation (ICCA), Dec. 2011, doi: 10.1109/icca.2011.6138029.

<a id="4">[4]</a>“Towards the development of an intuitive teleoperation system for human support robot using a VR device,” Advanced Robotics, 2020. https://www.tandfonline.com/doi/full/10.1080/01691864.2020.1813623 (accessed Jun. 08, 2022).

<a id="5">[5]</a>“Real-time gesture recognition for the high-level teleoperation interface of a mobile manipulator | Proceedings of the 2014 ACM/IEEE international conference on Human-robot interaction,” ACM Conferences, 2014. https://dl.acm.org/doi/10.1145/2559636.2563712 (accessed Jun. 08, 2022).

<a id="6">[6]</a>K. Y. H. Lim, P. Zheng, and C.-H. Chen, “A state-of-the-art survey of Digital Twin: techniques, engineering product lifecycle management and business innovation perspectives,” Journal of Intelligent Manufacturing, vol. 31, no. 6, pp. 1313–1337, Nov. 2019, doi: 10.1007/s10845-019-01512-w.

<a id="7">[7]</a>I. A. Tsokalo, D. Kuss, I. Kharabet, F. H. P. Fitzek, and M. Reisslein, “Remote Robot Control with Human-in-the-Loop over Long Distances Using Digital Twins,” 2019 IEEE Global Communications Conference (GLOBECOM), Dec. 2019, doi: 10.1109/globecom38437.2019.9013428.

<a id="8">[8]</a>V. KUTS, T. OTTO, T. TAHEMAA, and Y. BONDARENKO, “DIGITAL TWIN BASED SYNCHRONISED CONTROL AND SIMULATION OF THE INDUSTRIAL ROBOTIC CELL USING VIRTUAL REALITY,” Publisherspanel.com, no. Journal of Machine Engineering 2019; 19 (1): 128-144, Feb. 2019, doi: 10.5604/01.3001.0013.0464.

<a id="9">[9]</a>P. Ponomareva, D. Trinitatova, A. Fedoseev, I. Kalinov, and D. Tsetserukou, “GraspLook: a VR-based Telemanipulation System with R-CNN-driven Augmentation of Virtual Environment,” 2021 20th International Conference on Advanced Robotics (ICAR), Dec. 2021, doi: 10.1109/icar53236.2021.9659460.

<a id="10">[10]</a>J. Chen, Z. Ji, H. Niu, R. Setchi, and C. Yang, “An Auto-Correction Teleoperation Method for a Mobile Manipulator Using Gaze Tracking and Hand Motion Detection,” Towards Autonomous Robotic Systems, pp. 422–433, 2019, doi: 10.1007/978-3-030-25332-5_36.

<a id="11">[11]</a>B. Bejczy et al., “Mixed Reality Interface for Improving Mobile Manipulator Teleoperation in Contamination Critical Applications,” Procedia Manufacturing, vol. 51, pp. 620–626, 2020, doi: 10.1016/j.promfg.2020.10.087.

<a id="12">[12]</a>“Inertial motion capture based reference trajectory generation for a mobile manipulator | Proceedings of the 2014 ACM/IEEE international conference on Human-robot interaction,” ACM Conferences, 2014. https://dl.acm.org/doi/10.1145/2559636.2559812 (accessed Jun. 14, 2022).

<a id="13">[13]</a>A. Carfi, C. Motolese, B. Bruno, and F. Mastrogiovanni, “Online Human Gesture Recognition using Recurrent Neural Networks and Wearable Sensors,” 2018 27th IEEE International Symposium on Robot and Human Interactive Communication (RO-MAN), Aug. 2018, doi: 10.1109/roman.2018.8525769.

<a id="14">[14]</a>I. Stančić, J. Musić, and T. Grujić, “Gesture recognition system for real-time mobile robot control based on inertial sensors and motion strings,” Engineering Applications of Artificial Intelligence, vol. 66, pp. 33–48, Nov. 2017, doi: 10.1016/j.engappai.2017.08.013.

<a id="15">[15]</a>S. Ikegami, C. Premachandra, B. H. Sudantha, and S. Sumathipala, “A Study on Mobile Robot Control by Hand gesture Detection,” 2018 3rd International Conference on Information Technology Research (ICITR), Dec. 2018, doi: 10.1109/icitr.2018.8736135.

<a id="16">[16]</a>P. V. Kulkarni, B. Illing, B. Gaspers, B. Brüggemann, and D. Schulz, “Mobile manipulator control through gesture recognition using IMUs and Online Lazy Neighborhood Graph search,” ACTA IMEKO, vol. 8, no. 4, p. 3, Dec. 2019, doi: 10.21014/acta_imeko.v8i4.677.

<a id="17">[17]</a>M. Jain et al., “Object Detection and Gesture Control of Four-Wheel Mobile Robot,” 2019 International Conference on Communication and Electronics Systems (ICCES), Jul. 2019, doi: 10.1109/icces45898.2019.9002323.

<a id="18">[18]</a>B. Marcheix, B. Gardiner, and S. Coleman, “Adaptive Gesture Recognition System for Robotic Control using Surface EMG Sensors,” 2019 IEEE International Symposium on Signal Processing and Information Technology (ISSPIT), Dec. 2019, doi: 10.1109/isspit47144.2019.9001765.

<a id="19">[19]</a>S. Waldherr, R. Romero, and S. Thrun, “A Gesture Based Interface for Human-Robot Interaction,” Autonomous Robots, vol. 9, no. 2, pp. 151–173, 2000, doi: 10.1023/a:1008918401478.

<a id="20">[20]</a>A. Suresh, D. Gaba, S. Bhambri, and D. Laha, “Intelligent Multi-fingered Dexterous Hand Using Virtual Reality (VR) and Robot Operating System (ROS),” Robot Intelligence Technology and Applications 5, pp. 459–474, May 2018, doi: 10.1007/978-3-319-78452-6_37.

<a id="21">[21]</a>“Mobile manipulator control based on voice and visual signal,” Ieee.org, 2013. https://ieeexplore.ieee.org/document/6640172 (accessed Jun. 14, 2022).

<a id="22">[22]</a>D. Chalagulla, J. Jayateertha, T. Giri, and V. Sailaja, “Gesture Controlled Bomb Diffusing Mobile Robot,” 2018 Second International Conference on Intelligent Computing and Control Systems (ICICCS), Jun. 2018, doi: 10.1109/iccons.2018.8662838.

<a id="23">[23]</a>E. Coronado, J. Villalobos, B. Bruno, and F. Mastrogiovanni, “Gesture-based robot control: Design challenges and evaluation with humans,” 2017 IEEE International Conference on Robotics and Automation (ICRA), May 2017, doi: 10.1109/icra.2017.7989321.

<a id="24">24]</a>A. Carfi and F. Mastrogiovanni, “Gesture-Based Human-Machine Interaction: Taxonomy, Problem Definition, and Analysis,” IEEE Transactions on Cybernetics, pp. 1–17, 2021, doi: 10.1109/tcyb.2021.3129119.
